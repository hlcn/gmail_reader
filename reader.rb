require 'bundler'

# Loads all gems from the Gemfile
Bundler.require(:default)
Dotenv.load

user = ENV['GMAIL_USERNAME']
password = ENV['GMAIL_PASSWORD']

print "\n> Connecting..."
gmail = Gmail.connect(user, password)

print "Done.\n"

unread_count = gmail.inbox.emails(:unread).count
puts "> Unread mails: #{unread_count}"

if unread_count > 0
  gmail.inbox.find(:unread).each do |mail|
    mail.read!
    unless mail.attachments.empty?
      puts "> Found #{mail.attachments.size} file(s) on mail ##{mail.uid}..."
      mail.attachments.each do |attachment|
        print ".. Saving file \"#{mail.uid}_#{attachment.filename}\"..."
        File.open("./attachments/#{mail.uid}_#{attachment.filename}", "wb") do |file|
          file.write(attachment.body.decoded)
        end
        print "Done.\n"
      end
    end
  end
else
  puts "> Nothing to do here."
end

print "> Logging out..."
gmail.logout
print "Done.\n"
