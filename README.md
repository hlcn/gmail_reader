# Gmail Reader

Simple library to read emails from a Gmail account.

### Before getting started

You need to create an `.env` file which will contain the environment variables to connect. 
Please note how the **quotes are present**.

```
# Sample .env file

GMAIL_USERNAME="your_account@gmail.com"
GMAIL_PASSWORD="your_password"
```

### How to run:

```
# Build the image
$ docker build . -t gmail_reader

# Now run the image
$ docker run \
--interactive --tty --rm \
--volume /var/run/docker.sock:/var/run/docker.sock \
--volume "$PWD":/usr/src/app \
gmail_reader
```

Good luck!
